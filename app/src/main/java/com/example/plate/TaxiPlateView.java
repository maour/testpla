package com.example.plate;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;


public class TaxiPlateView extends LinearLayoutCompat {

    private final static String TAG = "TaxiPlateView";

    @Bind(R.id.txtPlate)
    EditText num1;
    @Bind(R.id.txtPlate2)
    EditText num2;
    @Bind(R.id.txtPlate3)
    EditText num3;

    public TaxiPlateView(Context context) {
        this(context, null);
    }

    public TaxiPlateView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaxiPlateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(context).inflate(R.layout.view_plate_taxi, this);
        ButterKnife.bind(this, view);


        setBackgroundResource(R.drawable.taxi_plate);


        num1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 1) num2.requestFocus();
            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 2) num3.requestFocus();
            }
        });

//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TaxiPlateView, 0, 0);
//        String num1 = a.getString(R.styleable.TaxiPlateView_number1left);
//        String num2 = a.getString(R.styleable.TaxiPlateView_number2middle);
//        String num3 = a.getString(R.styleable.TaxiPlateView_number3right);
//
//        Log.i(TAG, "TaxiPlateView: " + num1);
//
//        setOrientation(LinearLayoutCompat.HORIZONTAL);
//        setGravity(GravityCompat.START);
//        LayoutInflater inflater = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        inflater.inflate(R.layout.view_plate_taxi, this, true);
//        a.recycle();

    }

    public String getPlateNumber() {
        return num1.getText().toString() + num2.getText().toString() + num3.getText().toString();
    }
}
